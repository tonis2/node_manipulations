mod elements;

use elements::Text;
use tree::Node;

#[test]
fn create_element() {
    let mut main: Node = Text {
        data: "main".into(),
    }
    .into();

    assert_eq!(main.content().children.borrow().len(), 0);

    let mut child: Node = Text {
        data: "child".into(),
    }
    .into();

    main.set_id("parent");
    child.set_id("child");

    assert_eq!(main.id(), "parent".to_string());
    assert_eq!(child.id(), "child".to_string());

    main.add_event(Box::new(|node| {
        let parent = node.parent();
        assert!(parent.is_none());
    }));

    child.add_event(Box::new(|node| {
        let parent = node.clone().parent();
        assert!(&parent.is_some());

        node.get_as::<Text>(&mut |node| node.data = "changed".to_string());
    }));

    main.append(child);

    assert_eq!(main.content().children.borrow().len(), 1);

    main.handle_events();

    main.iter(&mut |node| {
        for child in node.content().children.borrow_mut().iter_mut() {
            child.handle_events();
            child.get_as::<Text>(&mut |node| assert_eq!(node.data, "changed".to_string()));
        }
    });
}
