use std::any::Any;

use tree::{Element, Node, NodeData, RenderResult, VertexData};

pub struct Text {
    pub data: String,
}

impl Element for Text {
    fn as_any(&mut self) -> &mut dyn Any {
        self
    }

    fn render(&self) -> RenderResult {
        VertexData {
            vertices: Vec::new(),
            indices: Vec::new(),
        }
        .into()
    }
}

impl Into<Node> for Text {
    fn into(self) -> Node {
        NodeData::new(self).into()
    }
}

impl Into<RenderResult> for Text {
    fn into(self) -> RenderResult {
        Node::from(NodeData::new(self)).into()
    }
}
