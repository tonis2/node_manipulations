use std::any::Any;
use std::cell::RefCell;
use std::rc::{Rc, Weak};

pub type Component = Rc<RefCell<dyn Element>>;

type NodeWeak = Weak<NodeData>;
type NodeRC = Rc<NodeData>;

type EventHandler = Box<dyn FnMut(NodeRC)>;
type IterCallback<'a> = &'a mut dyn FnMut(&mut Node);

pub struct Node {
    data: NodeRC,
    events: Vec<EventHandler>,
}

pub struct NodeData {
    pub(crate) id: RefCell<String>,
    pub(crate) parent: RefCell<NodeWeak>,
    pub(crate) content: Component,
    pub children: RefCell<Vec<Node>>,
}

impl Node {
    pub fn set_id(&mut self, id: &str) {
        self.data.id.replace(id.to_string());
    }

    pub fn id(&self) -> String {
        self.data.id.borrow().to_string()
    }

    pub fn handle_events(&mut self) {
        for event in self.events.iter_mut() {
            event(Rc::clone(&self.data));
        }
    }

    pub fn content(&self) -> NodeRC {
        Rc::clone(&self.data)
    }

    pub fn add_event(&mut self, event: EventHandler) {
        self.events.push(event);
    }

    pub fn append(&mut self, child_node: Node) {
        child_node
            .data
            .parent
            .replace(Rc::downgrade(&Rc::clone(&self.data)));

        self.data.children.borrow_mut().push(child_node);
    }

    pub fn get_as<T: Any>(&self, callback: &mut dyn FnMut(&mut T)) {
        let mut content = self.data.content.borrow_mut();
        callback(content.as_any().downcast_mut::<T>().unwrap())
    }

    pub fn iter(&mut self, callback: IterCallback) {
        self.iter_helper(callback);
    }

    fn iter_helper(&mut self, callback: IterCallback) {
        callback(self);
        for child in self.data.children.borrow_mut().iter_mut() {
            child.iter_helper(callback)
        }
    }
}

impl NodeData {
    pub fn new<T: Element>(node: T) -> Self {
        Self {
            id: RefCell::new("default".into()),
            parent: RefCell::new(Weak::default()),
            children: RefCell::new(Vec::new()),
            content: Rc::new(RefCell::new(node)),
        }
    }

    pub fn id(&self) -> String {
        self.id.borrow().to_string()
    }

    pub fn get_as<T: Any>(&self, callback: &mut dyn FnMut(&mut T)) {
        let mut content = self.content.borrow_mut();
        callback(content.as_any().downcast_mut::<T>().unwrap())
    }

    pub fn parent(self: Rc<NodeData>) -> Option<NodeRC> {
        self.parent.borrow_mut().upgrade()
    }

    pub fn append(self: NodeRC, child_node: Node) {
        child_node
            .data
            .parent
            .replace(Rc::downgrade(&Rc::clone(&self)));

        self.children.borrow_mut().push(child_node)
    }
}

pub trait Element: Any {
    fn as_any(&mut self) -> &mut dyn Any;
    fn render(&self) -> RenderResult;
}

impl From<NodeData> for Node {
    fn from(content: NodeData) -> Node {
        Node {
            data: Rc::new(content),
            events: Vec::new(),
        }
    }
}

impl std::fmt::Debug for NodeData {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(fmt, "NodeData {{ id: {:?} }}", self.id,)
    }
}

impl std::fmt::Debug for Node {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let id = self.data.id.borrow().clone();
        write!(fmt, "Node {{ id: {:?} }}", id)
    }
}

pub struct Vertex {}

pub struct VertexData {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
}

pub enum RenderResult {
    Node(Node),
    Vertices(VertexData),
}

impl From<Node> for RenderResult {
    fn from(result: Node) -> Self {
        RenderResult::Node(result)
    }
}

impl From<VertexData> for RenderResult {
    fn from(result: VertexData) -> Self {
        RenderResult::Vertices(result)
    }
}
